__author__ = 'searcher'

import socket


# method to return the first available port
class AvailablePorts:
    def __init__(self):
        pass

    @staticmethod
    def get_available_port():
        # s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_IP)
        # s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 0))
        port = s.getsockname()[1]
        s.close()
        return port

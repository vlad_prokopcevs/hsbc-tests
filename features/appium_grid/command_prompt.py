import shlex
import subprocess
import sys
import os
from threading import Thread
from Queue import Queue, Empty
import signal
import collections

ON_POSIX = 'posix' in sys.builtin_module_names

__author__ = 'searcher'

from subprocess import Popen, check_output, PIPE
import platform


class Alarm(Exception):
    pass


def alarm_handler(signum, frame):
    raise Alarm


class CommandPrompt:
    def __init__(self):
        pass

    def enqueue_output(self, out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()

    @staticmethod
    def run_command(command):
        """
        :rtype : byte string
        """
        op_sys = platform.system()

        # build cmd process according to os
        if "Windows" in op_sys:  # .find("Windows") != -1:
            return_code = Popen(command.split(), stdout=PIPE).communicate()[0]
        elif "Darwin" in op_sys:
            signal.signal(signal.SIGALRM, alarm_handler)
            signal.alarm(30)
            try:
                return_code = check_output(shlex.split(command))
                signal.alarm(0)  # reset the alarm
            except Alarm:
                print "Oops, taking too long!"
                raise
        else:
            return_code = check_output(command.split())

        return return_code

    def command_pipes_decompose_recur(self, command, level=0):
        #
        # Recursive method to split the command into several commands, that use the output of each other
        # if the initial command contains pipes.
        #
        """
        :rtype : Popen instance
        @param level:
        @param command:
        """
        if "|" in command:
            cmd_left, cmd_right = command.rsplit("|", 1)
            output = subprocess.Popen(shlex.split(cmd_right),
                                      stdin=self.command_pipes_decompose_recur(cmd_left, level + 1).stdout,
                                      stdout=subprocess.PIPE,
                                      bufsize=1, close_fds=ON_POSIX,
                                      stderr=subprocess.STDOUT)
        else:
            output = subprocess.Popen(shlex.split(command, level + 1), stdout=subprocess.PIPE,
                                      bufsize=1, close_fds=ON_POSIX,
                                      stderr=subprocess.STDOUT)
        return output

    def run_command_ser(self, command):
        #
        # This method launches the command and returns its output.
        # Before launching the command is passed to command_pipes_decompose_recur to decompose in case it has pipes.
        # Subprocesses are launched in serial mode (one after another, not in parallel)
        #
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(30)
        output = self.command_pipes_decompose_recur(command, 0)
        number_of_lines = 200
        stdout_output = collections.deque(maxlen=number_of_lines)
        # stderr_output = collections.deque(maxlen=number_of_lines)
        try:
            # save last `number_of_lines` lines of the process output
            for line in iter(output.stdout.readline, ""):
                stdout_output.append(line)
            signal.alarm(0)  # reset the alarm
        except Alarm:
            print "Oops, taking too long!"
            output.terminate()
            # raise
        finally:
            # print saved lines
            return ''.join(stdout_output), output.stderr

    def run_command_parallel(self, command):
        line = ""
        output = self.command_pipes_decompose_recur(command, 0)
        q = Queue()
        t = Thread(target=self.enqueue_output, args=(output.stdout, q))
        t.daemon = True  # thread dies with the program
        t.start()
        try:
            line = q.get_nowait()  # or q.get(timeout=.1)
        except Empty:
            print('no output yet')
            line = 'no output yet'
        else:  # got line
            print (line)
            if "not" in line:
                print("\nCould not execute the command")
                sys.exit(0)
        return line

        # def run_command_par_recur(self, command):
        #     """
        #     :rtype : byte string
        #     """
        #     final_output = ""
        #     if "|" in command:
        #         cmd_left, cmd_right = command.rsplit("|", 1)
        #         output = subprocess.Popen(shlex.split(cmd_right), stdin=self.run_command_par_recur(cmd_left),
        #                                   stdout=subprocess.PIPE,
        #                                   bufsize=1, close_fds=ON_POSIX,
        #                                   stderr=subprocess.STDOUT)
        #     else:
        #         output = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
        #                                   bufsize=1, close_fds=ON_POSIX,
        #                                   stderr=subprocess.STDOUT)
        #     print ("This is debug of appium manager process checker")
        #     q = Queue()
        #     t = Thread(target=self.enqueue_output, args=(output.stdout, q))
        #     t.daemon = True  # thread dies with the program
        #     t.start()
        #     try:
        #         line = q.get_nowait()  # or q.get(timeout=.1)
        #     except Empty:
        #         print('no output yet')
        #     else:  # got line
        #         print (line)
        #         final_output = final_output + line
        #         if "error" in line:
        #             print("\nThere was an error in the command execution")
        #     return final_output

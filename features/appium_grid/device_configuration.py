__author__ = 'searcher'

import sys
from command_prompt import CommandPrompt
import re


class DeviceConfiguration:
    cmd = CommandPrompt()

    def __init__(self):
        self.devices = dict({})
        pass

    def device_name(slef, device):
        m = re.compile("([^\(]+)").match(device)
        return m.groups()[0] if m else None

    def os_version(self, device):
        m = re.compile("\(((\d\.)*\d\s*.*?)\)").search(device)
        return m.groups()[0] if m else None

    def device_id(self, device):
        m = re.compile("\[([^\]]+)\]").search(device)
        return m.groups()[0] if m else None

    def start_adb(self):
        #
        # This method starts adb server
        #
        out = self.cmd.run_command("adb start-server")
        lin = out.splitlines()
        if len(lin) == 0:
            print ("adb service already started\n")
        elif lin[1].lower == "* daemon started successfully *":
            print ("adb service started")
        elif "internal or external command" in lin[0] or "command not found" in lin[0]:
            print ("adb path not set in sys variable")
            sys.exit(0)

    def stop_adb(self):
        # /**
        #  * This method stops adb server
        #  */
        self.cmd.run_command("adb kill-server")
        pass

    def get_devices(self, device_type="andr"):
        # '''
        # This method return connected devices
        # @ return hashmap of connected devices information
        # '''
        """

        :param device_type: string
        :rtype : list
        """
        if device_type == "andr":  # Android devices detection
            # start adb service
            self.start_adb()
            out = self.cmd.run_command_ser("adb devices")
            output_lines = out[0].splitlines()

            # exit if no connected devices found
            if len(output_lines) <= 1:
                print ("No Android device connected.")
                self.stop_adb()
                # sys.exit(0)
                return self.devices

            for i in range(1, len(output_lines) - 1):
                output_lines[i] = output_lines[i].replace(' ', '')
                if "device" in output_lines[i]:
                    output_lines[i] = output_lines[i].replace("\tdevice", "")
                    device_id = output_lines[i]
                    model = self.cmd.run_command("adb -s " + device_id + " shell getprop ro.product.model").strip(
                        " ").strip("\n\r")
                    brand = self.cmd.run_command("adb -s " + device_id + " shell getprop ro.product.brand").strip(
                        " ").strip("\n\r")
                    os_version = self.cmd.run_command(
                        "adb -s " + device_id + " shell getprop ro.build.version.release").replace(' ', '').strip(
                        "\n\r")
                    device_name = brand + " - " + model
                    self.devices["platform_name" + str(i)] = 'Android'
                    self.devices["device_id" + str(i)] = device_id
                    self.devices["device_name" + str(i)] = device_name
                    self.devices["os_version" + str(i)] = os_version

                    print ("Following device is connected:")
                    print (device_id + ";\t " + device_name + ";\t " + os_version + "\n")
                elif "unauthorized" in output_lines[i]:
                    device_id = output_lines[i].replace("unauthorized", "")
                    print ("Following device is unauthorized")
                    print (device_id + "\n")
                elif "offline" in output_lines[i]:
                    device_id = output_lines[i].replace("offline", "")
                    print ("Following device is offline")
                    print (device_id + "\n")
        else:  # iOS devices detection
            device_count = 0
            # look for connected ios devices
            out = self.cmd.run_command_ser("instruments -s devices")
            # "system_profiler SPUSBDataType | sed -n -e '/iPad/,/Serial/p' -e '/iPhone/,/Serial/p' -e '/iPod/,/Serial/p' | grep \"Serial Number:\" | awk -F \": \" '{print $2}'"

            output_lines = out[0].splitlines()
            # Output example:
            # Vlad's MacBook Pro [8BED6571-43E7-53FC-83DB-C609A2E8E326]
            #  iPhone 5 iOS 8.0.2 (8.0.2) [f06e7d09b32a6eb826c0e8ebae29705e33e4919c]
            # IPhone 5 iOS 8.0 (8.0) [8448cbda0642c2d0a2ce3839169777602700deb1]
            # iPad 2.0 iOS 8.1.2 (8.1.2) [4065840439d757a8a90f198e07f089728d206e57]
            # iPad Mini Retina WiFi 16GB (8.4) [201abc4c4224a66a9a1fbefd4e73e5e8916ec99c]
            # iPhone 4S iOS 8.2 (8.2) [8dcccaa98c05a88ebb174d3ded4ec36c92a1eb63]
            # Resizable iPad (8.4 Simulator) [2B03460B-FF3D-4A19-80C8-E45BEFD86B92]

            if len(output_lines) <= 1:
                # this will never happen
                print ("No iOS device connected.")
                return self.devices
            for i in range(1, len(output_lines) - 1):
                os_version = self.os_version(output_lines[i])
                if (os_version is None) or (os_version == ''):
                    print("Device {} does not look like a proper mobile iOS device. Skipping.".format(output_lines[i]))
                    continue
                elif 'Simulator' in os_version:
                    print (
                        "iOS simulator {} was found. I don't support simulators yet. Skipping.".format(output_lines[i]))
                    continue
                else:
                    device_id = self.device_id(output_lines[i])
                    if (device_id is None) or (device_id == ''):
                        print ("Device {} was found. Uid was not recognised. Skipping.".format(output_lines[i]))
                        continue
                    elif "-" in device_id:
                        print (
                            "Device {} was found. Uid does not look like belonging to a mobile device. Skipping.".format(
                                output_lines[i]))
                        continue
                    else:
                        device_name = self.device_name(output_lines[i])
                        self.devices["platform_name" + str(device_count + 1)] = 'iOS'
                        self.devices["device_id" + str(device_count + 1)] = device_id
                        self.devices["device_name" + str(device_count + 1)] = device_name
                        self.devices["os_version" + str(device_count + 1)] = os_version
                        print ("Following device is connected:")
                        print (device_id + ";\t " + device_name + ";\t " + os_version + "\n")
                        device_count += 1
        return self.devices



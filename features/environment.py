from urllib2 import URLError
from behave.runner import Context
from appium import webdriver
from features.appium_grid.appium_manager import AppiumManager
from globvar import *
from parse import *
import logging
import sys
from behave.log_capture import capture


def before_all(context):
    """
    @param context: Context

    """
    if "delay" in context.config.userdata:
        delay = int(context.config.userdata["delay"])
    else:
        delay = 0
    time.sleep(THINK_TIME + delay * 5)

    appium_manager = AppiumManager()
    port = appium_manager.start_appium()
    # port = '  4723'
    time.sleep(THINK_TIME * 3)
    print ("appium has been launched on port:" + str(port))

    if "platformName" in context.config.userdata:
        platform_name = context.config.userdata["platformName"].replace('\ ', ' ')
    else:
        platform_name = 'Android'  # 'iOS'  # 'Android'
    if "os_version" in context.config.userdata:
        os_version = context.config.userdata["os_version"]
    else:
        os_version = '5.0.2'  # '5.1.0'  # '5.0.2'

    if "device_id" in context.config.userdata:
        device_id = context.config.userdata["device_id"]
    else:
        device_id = 'TA93001AMU'  # '201abc4c4224a66a9a1fbefd4e73e5e8916ec99c'  # 'FA43EWM00781'  # "192.168.56.101:5555" #'04157df41149690c' # 'TA93001AMU'
    # c8ce4a364babdc50ce6b65d84579b1d1231d55c0

    if "app" in context.config.userdata:
        app = context.config.userdata["app"].replace('\ ', ' ')
    else:
        app = APK_PATH.replace('\ ', ' ')

    desired_caps = {'waitForAppScript': '$.delay(1000); true;',
                    'autoAcceptAlerts': 'true',
                    'platformName': platform_name,  # 'Android',
                    'platformVersion': os_version,  # '5.0.2',
                    'deviceName': device_id,  # 'TA93001AMU',  # 04157df41149690c
                    'udid': device_id,
                    'noReset': 'true',
                    'fullReset': 'false',
                    "newCommandTimeout": 1000,
                    # 'automationName': 'selendroid',
                    'app': app  # APK_PATH.replace('\ ', ' ')
                    }
    print ("desired caps: ")
    print (desired_caps)
    print ('http://localhost:' + str(port) + '/wd/hub')
    for i in range(0, 5):
        if appium_manager.is_server_running(False, port):
            context.driver = webdriver.Remote('http://localhost:' + str(port) + '/wd/hub', desired_caps)
            print ("Connected to Appium with {} from the {} time.".format(device_id, i + 1))
            break
            # I have not decided, weather I should catch exception here
            # or just making sure the appium server is running is enough to connect to it.
            # try:
            #     context.driver = webdriver.Remote('http://localhost:' + str(port) + '/wd/hub', desired_caps)
            # except:
            #     print ("Failed to connect to Appium with {} for the first time. Waiting to retry.".format(device_id))
            #     time.sleep(THINK_TIME + delay * 5)
            #     context.driver = webdriver.Remote('http://localhost:' + str(port) + '/wd/hub', desired_caps)
            # else:
            #     print ("Failed to connect to Appium with {} completely. Quiting.".format(device_id))
            #     temp = "Unexpected error: {}".format(sys.exc_info()[0])
            #     raise Exception(temp)
        else:
            print ("Failed to connect to Appium with {} for the {} time. Waiting to retry.".format(device_id, i))
            time.sleep(THINK_TIME + delay * 3)
    pass


def before_feature(context, feature):
    time.sleep(THINK_TIME)
    pass


def before_scenario(context, scenario):
    pass


@capture
def after_scenario(context, scenario):
    pass


def after_step(context, step):
    pass


def after_feature(context, feature):
    if context.config.userdata.get('test_type') == "selenium":
        context.browser.quit()


def after_all(context):
    # TODO: For every launched appium server there should be a tear down
    context.driver.quit()
    # logger.info("Driver quit during after_all")
    # killall node
    pass

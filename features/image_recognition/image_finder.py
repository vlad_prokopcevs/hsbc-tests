__author__ = 'searcher'

import numpy as np
import argparse
import imutils
import cv2
import os


class ImageFindException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class ImageFinder:
    def __init__(self):
        pass

    def find_images(self, queryimage_file, screenshot_file, threshold=593374000.0):
        # TM_CCOEFF threshold=34000000.0
        # TM_CCOEFF_NORMED threshold=0.4
        # TM_CCORR threshold=572949000.0
        """
        @param queryimage_file:
        @param screenshot_file:
        @return: coordinates tuple of the found image.
        """
        min_val = 0
        max_val = 0
        min_loc = 0
        max_loc = 0
        # load the screenshot and pattern images
        screenshot = cv2.imread(screenshot_file)
        if os.path.exists(os.path.normpath(queryimage_file)):
            pattern = cv2.imread(os.path.normpath(queryimage_file))
        pattern_height, pattern_width = pattern.shape[:2]

        # find the pattern in the screenshot
        result = cv2.matchTemplate(screenshot, pattern, cv2.TM_CCORR)

        # Check if the recognised image is within recognition threshold.
        while True:
            print (threshold)
            loc = np.where(result >= threshold)
            if any(map(len, loc)):
                min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
                top_left = max_loc
                bot_right = (top_left[0] + pattern_width, top_left[1] + pattern_height)
                for pt in zip(*loc[::-1]):
                    cv2.rectangle(screenshot, pt, (pt[0] + pattern_width, pt[1] - pattern_height), (0, 0, 255), 2)
                    # break

                    # cv2.imshow("Puzzle", imutils.resize(screenshot, height=650))
                    # cv2.imshow("Waldo", pattern)
                    # cv2.waitKey(0)
                    return top_left, bot_right
                break
            else:
                if threshold > 0:
                    threshold -= 100000.0
                else:
                    print ("Query image was not found")
                    raise ImageFindException("Query image was not found")
        # loc = np.where(result >= threshold)
        #
        # if any(map(len, loc)):
        #     min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

            # grab the bounding box of pattern and extract him from
            # the screenshot image
            # top_left = max_loc
            # bot_right = (top_left[0] + pattern_width, top_left[1] + pattern_height)

            # for pt in zip(*loc[::-1]):
            #     cv2.rectangle(screenshot, pt, (pt[0] + pattern_width, pt[1] - pattern_height), (0, 0, 255), 2)
            #     # break
            #
            # cv2.imshow("Puzzle", imutils.resize(screenshot, height=650))
            # cv2.imshow("Waldo", pattern)
            # cv2.waitKey(0)

            # return top_left, bot_right
        # else:
        #     print ("Query image was not found")
        #     raise ImageFindException("Query image was not found")


            # I use this code to test image recognition threashholds

            # for pt in zip(*loc[::-1]):
            #     cv2.rectangle(screenshot, pt, (pt[0] + pattern_width, pt[1] + pattern_height), (0,0,255), 2)
            #     # break
            #
            # cv2.imshow("Puzzle", imutils.resize(screenshot, height=650))
            # cv2.imshow("Waldo", pattern)
            # cv2.waitKey(0)

            # TODO: Delete the stuff commented below. It highlights the most likely location of a recognised pattern.
            # Similar to the code above, but instead of drawing borders around all possible matches it only shows
            # the most likely one by darkening the rest of the image.

            # min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
            #
            # # grab the bounding box of pattern and extract him from
            # # the screenshot image
            # top_left = max_loc
            # bot_right = (top_left[0] + pattern_width, top_left[1] + pattern_height)
            # roi = screenshot[top_left[1]:bot_right[1], top_left[0]:bot_right[0]]
            #
            #
            #
            # # construct a darkened transparent 'layer' to darken everything
            # # in the screenshot except for pattern
            # mask = np.zeros(screenshot.shape, dtype="uint8")
            # screenshot = cv2.addWeighted(screenshot, 0.25, mask, 0.75, 0)
            #
            # # put the original pattern back in the image so that he is
            # # 'brighter' than the rest of the image
            # screenshot[top_left[1]:bot_right[1], top_left[0]:bot_right[0]] = roi
            #
            # # display the images
            # cv2.imshow("Puzzle", imutils.resize(screenshot, height=650))
            # cv2.imshow("Waldo", pattern)
            # cv2.waitKey(0)
            # return top_left, bot_right


def main():
    image_finder = ImageFinder()
    print image_finder.find_images(
        "/Users/searcher/Documents/BitBucket/layar_client_qa/features/testcases/images_to_search/clean_green_button.jpg",
        "/Users/searcher/Documents/BitBucket/layar_client_qa/features/testcases/images_to_search/iPad2_green.png")


if __name__ == '__main__':
    main()

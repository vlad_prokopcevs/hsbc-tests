import os
import tempfile
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver import Remote
from selenium.common.exceptions import WebDriverException
from selenium.webdriver import TouchActions
from features.globvar import SCREENSHOT_DIR
from features.image_recognition.image_finder import *
from image_size import get_image_size

__author__ = 'searcher'


def find_image(driver, queryimage_file, screenshot_match=None):
    """
    @param driver  Remote
    @type queryimage_file str
    @type screenshot_match str
    @type driver: Remote
    @return:
    """
    print('Trying to find %s in current screen' % queryimage_file)

    fd, screenshot_file = tempfile.mkstemp()
    try:
        os.close(fd)
        # take screenshot from device
        head, tail = os.path.split(screenshot_file)
        screenshot(driver, tail, path=head)
        screenshot_width, screenshot_height = get_image_size("%s.png" % screenshot_file)
        screen_size = driver.get_window_size()
        screen_width = screen_size["width"]
        screen_height = screen_size["height"]
        image_finder = ImageFinder()
        top_left, bottom_right = image_finder.find_images(queryimage_file,
                                                          "%s.png" % screenshot_file
                                                          )
        # iOS hack: screenshot taken here may be twice as big as the actual screen size
        # and we need the real element position, not its position on the screenshot.
        if screenshot_width > screen_width:
            top_left = tuple(x / 2 for x in top_left)
            bottom_right = tuple(x / 2 for x in bottom_right)
        # head, tail = os.path.split(queryimage_file)
        # screenshot_match = '%s/%s-match.png' % (SCREENSHOT_DIR, tail.split('.')[0])
        return top_left, bottom_right
    except ImageFindException:
        raise ImageFindException("Query image was not found")

    finally:
        os.remove(screenshot_file)


def screenshot(driver, name, path=None):
    """
    @param driver:
    @param name:
    @param path:
    @type driver: Remote
    @return:
    """
    full_path = None
    if path:
        full_path = '%s/%s.png' % (path, name)
    else:
        full_path = '%s/%s.png' % (SCREENSHOT_DIR, name)

    try:
        driver.save_screenshot(full_path)
    except WebDriverException:  # for iOS, sometimes times out, so retry!
        # print("Failed taking screenshot %s - retrying" % full_path)
        driver.save_screenshot(full_path)
    width, height = get_image_size(full_path)
    if height < width:
        if driver.capabilities.get("platformName") == 'Android':
            # print("Rotating screenshot 270 degrees")
            os.system('sips -r 270 %s >/dev/null 2&>1' % full_path)
        else:
            # print("Rotating screenshot 90 degrees")
            os.system('sips -r 90 %s >/dev/null 2&>1' % full_path)


def tap_image(driver, query_image,
              width_modifier=0.5, height_modifier=0.5, retries=2):
    """

    @param driver:
    @param query_image:
    @param width_modifier:
    @param height_modifier:
    @param retries:
    @type driver: Remote
    @return:
    """
    retries_left = retries
    top_left = None
    bottom_right = None
    # while retries_left > 0 and not rect:
    while True:
        try:
            top_left, bottom_right = find_image(driver, query_image)
            break
        except ImageFindException:
            if retries_left > 0:
                retries_left -= 1
            else:
                raise ImageFindException("Query image was not found")

    image_name = os.path.split(query_image)[1]
    if top_left:
        print("Image %s is on screen" % image_name)
        x = int(top_left[0] + (bottom_right[0] - top_left[0]) * width_modifier)
        y = int(top_left[1] + (bottom_right[1] - top_left[1]) * height_modifier) - 20
        # adding 20 pixels for toolbar, that is not considered a part of the app screen by Appium
        # when looking for element coordinates.
        print('%s button found at %s %s (%sx%s), tapping at %s %s' %
              (image_name, top_left[0], top_left[1], bottom_right[0], bottom_right[1], x, y))

        tap(driver, x, y)


def tap(driver, x, y):
    """

    @param driver:
    @param x:
    @param y:
    @type driver Remote
    @return:
    """
    # x /= 2
    # y /= 2
    print('Tapping at %s,%s' % (x, y))
    # driver.tap([(x, y)], 1)
    if driver.capabilities.get("platformName") == 'Android':
        # if driver.isSelendroid():
        if driver.capabilities.get("automationName") == "Selendroid":
            touch_actions = TouchActions(driver)
            touch_actions.tap_and_hold(x, y).release(x, y).perform()
        else:
            action = TouchAction(driver)
            action.press(x=x, y=y).release().perform()
    else:  # iOS
        action = TouchAction(driver)
        action.press(x=x, y=y).release().perform()
        #
        # if driver.capabilities.get("udid") == "8dcccaa98c05a88ebb174d3ded4ec36c92a1eb63":
        #     # Temporary hack
        #     # On iPhone 4S = res. 480 x 320 for taps but screenshots are 960 x 640
        #     action.press(x=int(x / 2), y=int(y / 2)).release().perform()
        # else:
        #     action.press(x=x, y=y).release().perform()
        # action.tap(x=x, y=y).perform()
        # action.press(x=int(x / 2), y=int(y / 2)).release().perform()

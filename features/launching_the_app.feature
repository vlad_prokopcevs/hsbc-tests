# Created by searcher at 21/12/15
Feature: Launching the App
  I believe that user
  Would like to be able to launch an app
  To be able to interact with the HSBC virtual guide
  And to verify that....

  Scenario: Intro Screen Elements
    Given the app is installed on the device
    When I launch the app
    Then user can see "text" "Welcome to HSBC in Hong Kong" in "small" size
    And user can see "text" "A Virtual Story" in "large" size
    And user can see "text" "- an..." "text" in "small" size
    And user can see "button" "Start the journey" in "small" size